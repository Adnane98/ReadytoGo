import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'au-bercail',
    pathMatch: 'full'
  },

 
  {
    path: 'au-bercail',
    loadChildren: () => import('./pages/au-bercail/au-bercail.module').then( m => m.AuBercailPageModule)
  },
  {
    path: 'nos-recettes',
    loadChildren: () => import('./pages/nos-recettes/nos-recettes.module').then( m => m.NosRecettesPageModule)
  },
  {
    path: 'demande-a-emporter',
    loadChildren: () => import('./pages/demande-a-emporter/demande-a-emporter.module').then( m => m.DemandeAEmporterPageModule)
  },
  {
    path: 'nos-horaires',
    loadChildren: () => import('./pages/nos-horaires/nos-horaires.module').then( m => m.NosHorairesPageModule)
  },
  {
    path: 'modes-de-paiement',
    loadChildren: () => import('./pages/modes-de-paiement/modes-de-paiement.module').then( m => m.ModesDePaiementPageModule)
  },
  {
    path: 'nous-contacter',
    loadChildren: () => import('./pages/nous-contacter/nous-contacter.module').then( m => m.NousContacterPageModule)
  },
  {
    path: 'ses-pizzas',
    loadChildren: () => import('./pages/ses-pizzas/ses-pizzas.module').then( m => m.SesPizzasPageModule)
  },
  {
    path: 'ses-burgers',
    loadChildren: () => import('./pages/ses-burgers/ses-burgers.module').then( m => m.SesBurgersPageModule)
  },
  {
    path: 'ses-desalterants',
    loadChildren: () => import('./pages/ses-desalterants/ses-desalterants.module').then( m => m.SesDesalterantsPageModule)
  },
  {
    path: 'ses-amuses',
    loadChildren: () => import('./pages/ses-amuses/ses-amuses.module').then( m => m.SesAmusesPageModule)
  },
  {
    path: 'compte',
    loadChildren: () => import('./pages/compte/compte.module').then( m => m.ComptePageModule)
  },
  {
    path: 'ses-verdures',
    loadChildren: () => import('./pages/ses-verdures/ses-verdures.module').then( m => m.SesVerduresPageModule)
  },
  {
    path: 'ses-gourmandises',
    loadChildren: () => import('./pages/ses-gourmandises/ses-gourmandises.module').then( m => m.SesGourmandisesPageModule)
  },
  {
    path: 'ses-menus',
    loadChildren: () => import('./pages/ses-menus/ses-menus.module').then( m => m.SesMenusPageModule)
  },
  {
    path: 'ses-coupons',
    loadChildren: () => import('./pages/ses-coupons/ses-coupons.module').then( m => m.SesCouponsPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
