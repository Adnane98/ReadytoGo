import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NosRecettesPage } from './nos-recettes.page';

const routes: Routes = [
  {
    path: '',
    component: NosRecettesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NosRecettesPageRoutingModule {}
