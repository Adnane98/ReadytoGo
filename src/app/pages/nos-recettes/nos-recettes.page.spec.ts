import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NosRecettesPage } from './nos-recettes.page';

describe('NosRecettesPage', () => {
  let component: NosRecettesPage;
  let fixture: ComponentFixture<NosRecettesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NosRecettesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NosRecettesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
