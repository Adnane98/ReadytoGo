import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NosRecettesPageRoutingModule } from './nos-recettes-routing.module';

import { NosRecettesPage } from './nos-recettes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NosRecettesPageRoutingModule
  ],
  declarations: [NosRecettesPage]
})
export class NosRecettesPageModule {}
