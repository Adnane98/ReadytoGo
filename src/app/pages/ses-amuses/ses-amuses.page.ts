import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-ses-amuses',
  templateUrl: './ses-amuses.page.html',
  styleUrls: ['./ses-amuses.page.scss'],
})
export class SesAmusesPage implements OnInit {

  constructor(private http: HttpClient) {}

  amusesList: any;


  ngOnInit() {


  }

  ionViewDidEnter() {
    
    this.getAmuses()
    
  }
  
  getAmuses() {
      this.http.get('http://localhost:3000/amuse', {}).subscribe(data => {
        this.amusesList = data
      })
  }

}

