import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SesAmusesPage } from './ses-amuses.page';

const routes: Routes = [
  {
    path: '',
    component: SesAmusesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SesAmusesPageRoutingModule {}
