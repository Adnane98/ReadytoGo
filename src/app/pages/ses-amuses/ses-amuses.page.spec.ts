import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SesAmusesPage } from './ses-amuses.page';

describe('SesAmusesPage', () => {
  let component: SesAmusesPage;
  let fixture: ComponentFixture<SesAmusesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesAmusesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SesAmusesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
