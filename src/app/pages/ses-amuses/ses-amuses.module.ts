import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SesAmusesPageRoutingModule } from './ses-amuses-routing.module';

import { SesAmusesPage } from './ses-amuses.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SesAmusesPageRoutingModule
  ],
  declarations: [SesAmusesPage]
})
export class SesAmusesPageModule {}
