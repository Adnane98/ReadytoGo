import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SesGourmandisesPage } from './ses-gourmandises.page';

describe('SesGourmandisesPage', () => {
  let component: SesGourmandisesPage;
  let fixture: ComponentFixture<SesGourmandisesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesGourmandisesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SesGourmandisesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
