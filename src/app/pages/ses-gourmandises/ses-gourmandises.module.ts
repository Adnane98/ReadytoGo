import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SesGourmandisesPageRoutingModule } from './ses-gourmandises-routing.module';

import { SesGourmandisesPage } from './ses-gourmandises.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SesGourmandisesPageRoutingModule
  ],
  declarations: [SesGourmandisesPage]
})
export class SesGourmandisesPageModule {}
