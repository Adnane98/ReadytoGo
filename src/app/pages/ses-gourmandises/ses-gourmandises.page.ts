import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-ses-gourmandises',
  templateUrl: './ses-gourmandises.page.html',
  styleUrls: ['./ses-gourmandises.page.scss'],
})
export class SesGourmandisesPage implements OnInit {

  constructor(private http: HttpClient) {}

  gourmandisesList: any;

  ngOnInit() {
  }
  ionViewDidEnter() {
    
    this.getGourmandises()
    
  }
  getGourmandises() {
    this.http.get('http://localhost:3000/gourmandise', {}).subscribe(data => {
      this.gourmandisesList = data
    })
}

}
