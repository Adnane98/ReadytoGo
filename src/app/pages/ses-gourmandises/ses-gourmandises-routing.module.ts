import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SesGourmandisesPage } from './ses-gourmandises.page';

const routes: Routes = [
  {
    path: '',
    component: SesGourmandisesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SesGourmandisesPageRoutingModule {}
