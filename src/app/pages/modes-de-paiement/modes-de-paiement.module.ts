import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModesDePaiementPageRoutingModule } from './modes-de-paiement-routing.module';

import { ModesDePaiementPage } from './modes-de-paiement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModesDePaiementPageRoutingModule
  ],
  declarations: [ModesDePaiementPage]
})
export class ModesDePaiementPageModule {}
