import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModesDePaiementPage } from './modes-de-paiement.page';

describe('ModesDePaiementPage', () => {
  let component: ModesDePaiementPage;
  let fixture: ComponentFixture<ModesDePaiementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModesDePaiementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModesDePaiementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
