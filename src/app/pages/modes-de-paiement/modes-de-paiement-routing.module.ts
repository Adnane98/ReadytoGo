import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModesDePaiementPage } from './modes-de-paiement.page';

const routes: Routes = [
  {
    path: '',
    component: ModesDePaiementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModesDePaiementPageRoutingModule {}
