import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-ses-verdures',
  templateUrl: './ses-verdures.page.html',
  styleUrls: ['./ses-verdures.page.scss'],
})
export class SesVerduresPage implements OnInit {

  constructor(private http: HttpClient) {}

  verduresList: any;

  ngOnInit() {
  }

  ionViewDidEnter() {
    
    this.getVerdures()
    
  }
  
  getVerdures() {
      this.http.get('http://localhost:3000/verdure', {}).subscribe(data => {
        this.verduresList = data
      })
  }


}
