import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SesVerduresPage } from './ses-verdures.page';

describe('SesVerduresPage', () => {
  let component: SesVerduresPage;
  let fixture: ComponentFixture<SesVerduresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesVerduresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SesVerduresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
