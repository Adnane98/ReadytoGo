import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SesVerduresPageRoutingModule } from './ses-verdures-routing.module';

import { SesVerduresPage } from './ses-verdures.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SesVerduresPageRoutingModule
  ],
  declarations: [SesVerduresPage]
})
export class SesVerduresPageModule {}
