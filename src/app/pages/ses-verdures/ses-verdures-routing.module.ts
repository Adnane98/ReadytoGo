import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SesVerduresPage } from './ses-verdures.page';

const routes: Routes = [
  {
    path: '',
    component: SesVerduresPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SesVerduresPageRoutingModule {}
