import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SesPizzasPage } from './ses-pizzas.page';

const routes: Routes = [
  {
    path: '',
    component: SesPizzasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SesPizzasPageRoutingModule {}
