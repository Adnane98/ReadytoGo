import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SesPizzasPageRoutingModule } from './ses-pizzas-routing.module';

import { SesPizzasPage } from './ses-pizzas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SesPizzasPageRoutingModule
  ],
  declarations: [SesPizzasPage]
})
export class SesPizzasPageModule {}
