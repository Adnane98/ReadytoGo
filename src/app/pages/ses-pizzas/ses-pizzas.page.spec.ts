import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SesPizzasPage } from './ses-pizzas.page';

describe('SesPizzasPage', () => {
  let component: SesPizzasPage;
  let fixture: ComponentFixture<SesPizzasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesPizzasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SesPizzasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
