import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-ses-pizzas',
  templateUrl: './ses-pizzas.page.html',
  styleUrls: ['./ses-pizzas.page.scss'],
})
export class SesPizzasPage implements OnInit {

  constructor(private http: HttpClient) {}

  pizzasList: any;


  ngOnInit() {


  }

  ionViewDidEnter() {
    
    this.getPizzas()
    
  }
  
  getPizzas() {
      this.http.get('http://localhost:3000/pizza', {}).subscribe(data => {
        this.pizzasList = data
      })
  }

}
