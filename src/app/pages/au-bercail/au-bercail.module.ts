import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuBercailPageRoutingModule } from './au-bercail-routing.module';

import { AuBercailPage } from './au-bercail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuBercailPageRoutingModule
  ],
  declarations: [AuBercailPage]
})
export class AuBercailPageModule {}
