import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuBercailPage } from './au-bercail.page';

const routes: Routes = [
  {
    path: '',
    component: AuBercailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuBercailPageRoutingModule {}
