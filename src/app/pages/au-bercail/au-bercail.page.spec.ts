import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AuBercailPage } from './au-bercail.page';

describe('AuBercailPage', () => {
  let component: AuBercailPage;
  let fixture: ComponentFixture<AuBercailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuBercailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AuBercailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
