import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemandeAEmporterPage } from './demande-a-emporter.page';

const routes: Routes = [
  {
    path: '',
    component: DemandeAEmporterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DemandeAEmporterPageRoutingModule {}
