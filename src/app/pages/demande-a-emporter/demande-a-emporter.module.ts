import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DemandeAEmporterPageRoutingModule } from './demande-a-emporter-routing.module';

import { DemandeAEmporterPage } from './demande-a-emporter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DemandeAEmporterPageRoutingModule
  ],
  declarations: [DemandeAEmporterPage]
})
export class DemandeAEmporterPageModule {}
