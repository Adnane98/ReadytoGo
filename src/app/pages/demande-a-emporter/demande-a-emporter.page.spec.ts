import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DemandeAEmporterPage } from './demande-a-emporter.page';

describe('DemandeAEmporterPage', () => {
  let component: DemandeAEmporterPage;
  let fixture: ComponentFixture<DemandeAEmporterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandeAEmporterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DemandeAEmporterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
