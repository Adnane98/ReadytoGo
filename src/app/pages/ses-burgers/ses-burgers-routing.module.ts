import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SesBurgersPage } from './ses-burgers.page';

const routes: Routes = [
  {
    path: '',
    component: SesBurgersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SesBurgersPageRoutingModule {}
