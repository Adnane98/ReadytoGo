import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-ses-burgers',
  templateUrl: './ses-burgers.page.html',
  styleUrls: ['./ses-burgers.page.scss'],
})
export class SesBurgersPage implements OnInit {


  constructor(private http: HttpClient) {}

  burgersList: any;

  ngOnInit() {
  }

  ionViewDidEnter() {
    
    this.getBurgers()
    
  }
  
  getBurgers() {
      this.http.get('http://localhost:3000/burger', {}).subscribe(data => {
        this.burgersList = data
      })
  }

}
