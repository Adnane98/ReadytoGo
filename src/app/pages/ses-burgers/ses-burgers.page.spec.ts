import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SesBurgersPage } from './ses-burgers.page';

describe('SesBurgersPage', () => {
  let component: SesBurgersPage;
  let fixture: ComponentFixture<SesBurgersPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesBurgersPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SesBurgersPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
