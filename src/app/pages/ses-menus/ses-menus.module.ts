import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SesMenusPageRoutingModule } from './ses-menus-routing.module';

import { SesMenusPage } from './ses-menus.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SesMenusPageRoutingModule
  ],
  declarations: [SesMenusPage]
})
export class SesMenusPageModule {}
