import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-ses-menus',
  templateUrl: './ses-menus.page.html',
  styleUrls: ['./ses-menus.page.scss'],
})
export class SesMenusPage implements OnInit {

  constructor(private http: HttpClient) {}

  menusList: any;

  ngOnInit() {
  }
  ionViewDidEnter() {
    
    this.getMenus()
    
  }
  getMenus() {
    this.http.get('http://localhost:3000/menu', {}).subscribe(data => {
      this.menusList = data
    })
}

}
