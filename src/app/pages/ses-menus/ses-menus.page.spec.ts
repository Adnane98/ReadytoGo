import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SesMenusPage } from './ses-menus.page';

describe('SesMenusPage', () => {
  let component: SesMenusPage;
  let fixture: ComponentFixture<SesMenusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesMenusPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SesMenusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
