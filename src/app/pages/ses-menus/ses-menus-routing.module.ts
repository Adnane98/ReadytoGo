import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SesMenusPage } from './ses-menus.page';

const routes: Routes = [
  {
    path: '',
    component: SesMenusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SesMenusPageRoutingModule {}
