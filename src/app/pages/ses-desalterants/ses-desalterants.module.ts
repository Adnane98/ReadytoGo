import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SesDesalterantsPageRoutingModule } from './ses-desalterants-routing.module';

import { SesDesalterantsPage } from './ses-desalterants.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SesDesalterantsPageRoutingModule
  ],
  declarations: [SesDesalterantsPage]
})
export class SesDesalterantsPageModule {}
