import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SesDesalterantsPage } from './ses-desalterants.page';

describe('SesDesalterantsPage', () => {
  let component: SesDesalterantsPage;
  let fixture: ComponentFixture<SesDesalterantsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesDesalterantsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SesDesalterantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
