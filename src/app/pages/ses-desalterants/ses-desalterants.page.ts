import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-ses-desalterants',
  templateUrl: './ses-desalterants.page.html',
  styleUrls: ['./ses-desalterants.page.scss'],
})
export class SesDesalterantsPage implements OnInit {

  constructor(private http: HttpClient) {}

  desalterantsList: any;


  ngOnInit() {


  }

  ionViewDidEnter() {
    
    this.getDesalterants()
    
  }
  
  getDesalterants() {
      this.http.get('http://localhost:3000/desalterant', {}).subscribe(data => {
        this.desalterantsList = data
      })
  }

}
