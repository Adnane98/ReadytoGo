import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SesDesalterantsPage } from './ses-desalterants.page';

const routes: Routes = [
  {
    path: '',
    component: SesDesalterantsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SesDesalterantsPageRoutingModule {}
