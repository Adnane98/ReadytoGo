import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NousContacterPage } from './nous-contacter.page';

describe('NousContacterPage', () => {
  let component: NousContacterPage;
  let fixture: ComponentFixture<NousContacterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NousContacterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NousContacterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
