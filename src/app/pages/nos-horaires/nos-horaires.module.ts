import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NosHorairesPageRoutingModule } from './nos-horaires-routing.module';

import { NosHorairesPage } from './nos-horaires.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NosHorairesPageRoutingModule
  ],
  declarations: [NosHorairesPage]
})
export class NosHorairesPageModule {}
