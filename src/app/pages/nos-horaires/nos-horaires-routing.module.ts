import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NosHorairesPage } from './nos-horaires.page';

const routes: Routes = [
  {
    path: '',
    component: NosHorairesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NosHorairesPageRoutingModule {}
