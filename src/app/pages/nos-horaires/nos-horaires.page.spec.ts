import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NosHorairesPage } from './nos-horaires.page';

describe('NosHorairesPage', () => {
  let component: NosHorairesPage;
  let fixture: ComponentFixture<NosHorairesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NosHorairesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NosHorairesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
