import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SesCouponsPageRoutingModule } from './ses-coupons-routing.module';

import { SesCouponsPage } from './ses-coupons.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SesCouponsPageRoutingModule
  ],
  declarations: [SesCouponsPage]
})
export class SesCouponsPageModule {}
