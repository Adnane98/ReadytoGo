import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SesCouponsPage } from './ses-coupons.page';

const routes: Routes = [
  {
    path: '',
    component: SesCouponsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SesCouponsPageRoutingModule {}
