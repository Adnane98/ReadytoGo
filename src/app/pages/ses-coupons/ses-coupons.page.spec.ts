import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SesCouponsPage } from './ses-coupons.page';

describe('SesCouponsPage', () => {
  let component: SesCouponsPage;
  let fixture: ComponentFixture<SesCouponsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesCouponsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SesCouponsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
